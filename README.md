tp1-ghostbusters
================
Nombres de los integrantes del Grupo:
-------------------------------------
Abinet German, Fernandez Lucas, Gamboa Marcelo y Ledesma Javier.

Desiciones de diseño tomadas:
-----------------------------
El proyecto tomo diferentes formas hasta alcanzar una mas clara.
En reglas generales fue necesario cambiar los if y switch repetitivos por enum con atributos, 
cabe destacar que el uso de esto nos facilito ampliamente tareas como por ejemplo
las reglas de ataque y defensa.
En lo que refiere al menu encontramos un uso positivo en crear una interfaz de menues,
permitiendonos abrirnos en un abanico de menues y trabajar mas comodamente cada uno por separado.

Descripcion de los archivos *.java:
-----------------------------------

IMenu.java: es utilizada como interface del resto de los menu. 

MenuInicial.java: muestra el menu principal, es el encargado dar las opciones de 
                  crear el juego, y elegir el turno de los jugadores. Ademas, 
                  verifica el fin de la partida.
                 

MenuCrearJuego.java: muestra el menu para crear el juego. Luego pide los datos del 
                     personaje, lo crea y le asigna un monstruo.
                     
MenuCrearMonstruos: muestra el menu para crear monstruos. Muestra las tipologias
                    al usuario para que este seleccion,luego si la seleccion es 
                    diferente a null se le asigna al monstruo.(cada monstruo puede
                    tener dos tipologias diferentes.)
                    
MenuAtaques.java: recibe los monstruos como atributos, muestra al
                  usuario que ataques puede realizar y permite atacar.
                  
Juego.java: posee de atributos a los monstruos y personajes. Se encarga de los set
            y get de los mismos segun corresponda.

Monstruo.java: posee las tipologias de los monstruos y al momento de la batalla
               comprueba cuanto daño va a realizarle al adversario y lo lleva a cabo.

Personaje.java: creaun personaje con un nombre y posee un get del personaje.

Tipologia.java: posee un enum con las tipologias disponibles, en base a las reglas
                de fortalezas y debilidades de cada tipo se le agregaron atributos.

Conclusiones:
-------------
Como conclusion principal el uso del Git al principio no fue necesario, 
pero a medida que se requirieron realizar mas cambios en diferentes areas fue de
mucha utilidad para lograr terminar el proyecto.
La otra  parte es que sin lo aprendido en este cuatrimestre el proyecto se hubiera
tornado muy extenso en lineas de codigo y repetitivo, el uso de herencia, 
polimorfismo e interface, nos permitio dejar un proyecto mucho mas claro y rendidor
tanto para trabajar y verlo nosotros, como para un tercero.
            
Instalación
-----------
Clonar este repo:

    $ git clone https://gitlab.com/untref-ayp2-ghostbusters/tp1-ghostbusters.git