package tipologias.test;

import org.junit.Assert;
import org.junit.Test;

import tiplogias.Tipologia;

public class TipologiaTest {
	
	
	@Test
	public void devuelveLaTipologiaCorrectaEnAtaque() {
		Assert.assertEquals(Tipologia.FUEGO, Tipologia.AGUA.getFuerteEnAtaque());
	}
	
	@Test
	public void devuelveLaTipologiaCorrectaEnDefensa() {
		Assert.assertEquals(Tipologia.FUEGO, Tipologia.AGUA.getFuerteEnDefensa());
	}

}
