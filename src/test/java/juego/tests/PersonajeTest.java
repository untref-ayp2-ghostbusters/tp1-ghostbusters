package juego.tests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import juego.Personaje;

public class PersonajeTest {
	
	private Personaje personajeInicial;
	
	@Before
	public void personajeInicial() {
		this.personajeInicial = new Personaje("Ash Ketchum");
	}
	
	@Test
	public void seCreeUnPersonaje() {
		Assert.assertNotNull(new Personaje("Ash Ketchum"));
	}
	
	@Test
	public void devuelveElNombreDelPersonaje() {
		Assert.assertEquals("Ash Ketchum", this.personajeInicial.getNombreDePersonaje());
	}

}
