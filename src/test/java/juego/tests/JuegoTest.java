package juego.tests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import juego.Juego;
import juego.Monstruo;

public class JuegoTest {

	private Juego juego;

	@Before
	public void personajeInicial() {
		this.juego = new Juego();
		//
	}

	@Test
	public void seCreaUnaInstanciaDeJuego() {
		Assert.assertNotNull(new Juego());
	}

	@Test
	public void seVerificaSiSeCreaUnPersonajeDesdeElJuego() {
		Assert.assertNotNull(juego.crearPersonaje("Ash"));
	}

	@Test
	public void asignarYComprobarMonstruos() {
		Monstruo monstruo1 = new Monstruo();
		Monstruo monstruo2 = new Monstruo();
		juego.setMonstruo(monstruo1, 1);
		juego.setMonstruo(monstruo2, 2);
		Assert.assertEquals(monstruo1, juego.getMonstruo(1));
		Assert.assertEquals(monstruo2, juego.getMonstruo(2));
	}
	
	@Test
	public void getPersonajeYMonstruoInexistenteDebeSerNulo() {
		//Assert.assertNull(juego.getPersonaje(3));
		Assert.assertNull(juego.getMonstruo(3));
	}
	
	@Test
	public void setPersonajeYMonstruoNoSonAsignadosEnNingunLado() {
		juego.setMonstruo(new Monstruo(), 3);
	}
}
