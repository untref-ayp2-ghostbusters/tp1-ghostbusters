package juego.tests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import juego.Monstruo;
import juego.Personaje;
import tiplogias.Tipologia;

public class MonstruoTest {

	private Monstruo monstruo1;
	private Monstruo monstruo2;
	private final double VIDA_TOTAL = 100;
	private final double DANIO_ATAQUE_SIMPLE = 10;
	private final double DANIO_ATAQUE_ESPECIAL = 15;
	private final double PORCENTAJE_DANIO_ATAQUE_SIMPLE = 0.2;
	private final double DANIO_FUERTE_EN_ATAQUE = DANIO_ATAQUE_ESPECIAL + (DANIO_ATAQUE_ESPECIAL * PORCENTAJE_DANIO_ATAQUE_SIMPLE);
	
	@Before
	public void personajeInicial() {
		this.monstruo1 = new Monstruo();
		this.monstruo1.setTipologiaPrincipal(Tipologia.FUEGO);
		this.monstruo1.setTipologiaSecundaria(Tipologia.TIERRA);
		this.monstruo2 = new Monstruo();
		this.monstruo2.setTipologiaPrincipal(Tipologia.AGUA);
		this.monstruo2.setTipologiaSecundaria(Tipologia.AIRE);
	}

	@Test
	public void seCreaUnMonstruo() {
		Assert.assertNotNull(new Monstruo());
	}

	@Test
	public void seVerificanLasTipologiasDelMonstruo() {
		Assert.assertEquals(Tipologia.FUEGO, monstruo1.getTipologiaPrincipal());
		Assert.assertEquals(Tipologia.TIERRA, monstruo1.getTipologiaSecundaria());
	}

	@Test
	public void seRealizaUnAtaqueSimpleYSeVerificaElDañoRealizado() {		
		final double danioEsperado = VIDA_TOTAL - DANIO_ATAQUE_SIMPLE;
		monstruo1.ataque(monstruo2);
		Assert.assertEquals(danioEsperado, monstruo2.getPuntosDeVida(), 0);
	}

	@Test
	public void ataquesLuegoDeLaMuerteDelMonstruoSigueDejandoCeroDeVida() {
		for (int i = 0; i < 12; i++) {
			monstruo1.ataque(monstruo2);
		}
		Assert.assertEquals(0, monstruo2.getPuntosDeVida(), 0);
	}

	@Test
	public void noSoyFuerteEnAtaqueYEnemigoEsFuerteEnDefensa() {
		double danioEsperado = DANIO_ATAQUE_ESPECIAL - (DANIO_ATAQUE_ESPECIAL * PORCENTAJE_DANIO_ATAQUE_SIMPLE);
		double vidaRestante = VIDA_TOTAL - danioEsperado;
		monstruo1.ataque(monstruo1.getTipologiaPrincipal(), monstruo2);
		Assert.assertEquals(vidaRestante, monstruo2.getPuntosDeVida(), 0);
	}

	@Test
	public void soyFuerteEnAtaqueYEnemigoEsFuerteEnDefensa() {
		double danioEsperado = DANIO_FUERTE_EN_ATAQUE - (DANIO_ATAQUE_ESPECIAL * PORCENTAJE_DANIO_ATAQUE_SIMPLE);
		double vidaRestante = VIDA_TOTAL - danioEsperado; 
		monstruo2.ataque(monstruo2.getTipologiaPrincipal(), monstruo1);
		Assert.assertEquals(vidaRestante, monstruo1.getPuntosDeVida(), 0);
	}

	@Test
	public void soyFuerteEnAtaqueYEnemigoNoEsFuerteEnDefensa() {
		double vidaRestante = VIDA_TOTAL - DANIO_FUERTE_EN_ATAQUE; 
		
		Monstruo monstruoEnemigo = new Monstruo();
		monstruoEnemigo.setTipologiaPrincipal(Tipologia.FUEGO);
		monstruoEnemigo.setTipologiaSecundaria(Tipologia.AGUA);
		
		monstruo2.ataque(monstruo2.getTipologiaSecundaria(), monstruoEnemigo);
		Assert.assertEquals(vidaRestante, monstruoEnemigo.getPuntosDeVida(), 0);
	}

	@Test
	public void seRealizaUnAtaqueEspecialAireYSeVerificaElDanioRealizado() {
		final double ENEMIGO_FUERTE_EN_DEFENSA = 12;
		double vidaRestante = VIDA_TOTAL - ENEMIGO_FUERTE_EN_DEFENSA;
		monstruo1.ataque(monstruo1.getTipologiaPrincipal(), monstruo2);
		Assert.assertEquals(vidaRestante, monstruo2.getPuntosDeVida(), 0);
	}

	@Test
	public void ataquesEspecialesLuegoDeLaMuerteDelMonstruoSigueDejandoCeroDeVida() {
		for (int i = 0; i < 8; i++) {
			monstruo1.ataque(monstruo2);
		}
		monstruo1.ataque(monstruo1.getTipologiaPrincipal(), monstruo2);
		monstruo1.ataque(monstruo1.getTipologiaPrincipal(), monstruo2);
		Assert.assertEquals(0, monstruo2.getPuntosDeVida(), 0);
	}

	@Test
	public void ataquesEspecialesTieneUnLimiteDeCuatroLuegoHaceCeroDeDanio() {
		for (int i = 0; i < 4; i++) {
			monstruo1.ataque(monstruo1.getTipologiaPrincipal(), monstruo2);
		}
		Assert.assertEquals(0, monstruo1.ataque(monstruo1.getTipologiaPrincipal(), monstruo2), 0);
	}

	@Test
	public void seVerificaQueElPersonajeDelMonstruoNoSeaNulo() {
		monstruo1.setPersonaje(new Personaje("Ash"));
		Assert.assertNotNull(monstruo1.getPersonaje());
	}

}
