package tiplogias;

/**
 * Tipologias que pueden tener los monstruos.
 */
public enum Tipologia {
	AGUA("FUEGO", "FUEGO"), 
	AIRE("AGUA", "TIERRA"), 
	FUEGO("TIERRA", "AIRE"), 
	TIERRA("AIRE", "AGUA");

	private final String fuerteEnAtaque;
	private final String fuerteEnDefensa;

	Tipologia(String fuerteEnAtaque, String fuerteEnDefensa) {
		this.fuerteEnAtaque = fuerteEnAtaque;
		this.fuerteEnDefensa = fuerteEnDefensa;
	}

	public Tipologia getFuerteEnAtaque() {
		return Tipologia.valueOf(this.fuerteEnAtaque);
	}

	public Tipologia getFuerteEnDefensa() {
		return Tipologia.valueOf(this.fuerteEnDefensa);
	}
}
