package menues;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

import interfaces.IMenu;
import juego.Juego;

public class MenuInicial implements IMenu {

	private Juego juego;
	private HashMap<Integer, IMenu> menues;

	public MenuInicial(Juego juego) {
		this.juego = juego;
		this.menues = new HashMap<Integer, IMenu>();
		llenarMenues();
	}

	private boolean terminoElJuego() {

		boolean monsturosCreados = this.juego.getMonstruo(1) != null && this.juego.getMonstruo(2) != null;
		boolean personajesCreados = this.juego.getMonstruo(1).getPersonaje() != null
				& this.juego.getMonstruo(2).getPersonaje() != null;

		boolean juegoCreado = monsturosCreados && personajesCreados;
		boolean algunMonstruoNoTieneVida = this.juego.getMonstruo(1).getPuntosDeVida() == 0
				|| this.juego.getMonstruo(2).getPuntosDeVida() == 0;

		return juegoCreado && algunMonstruoNoTieneVida;
	}

	private String getJugadorGanador() {
		String nombreGanador;

		if (this.juego.getMonstruo(1).getPuntosDeVida() == 0) {
			nombreGanador = this.juego.getMonstruo(2).getPersonaje().getNombreDePersonaje();
		} else {
			nombreGanador = this.juego.getMonstruo(1).getPersonaje().getNombreDePersonaje();
		}

		return nombreGanador;
	}

	private void llenarMenues() {
		this.menues.put(1, new MenuCrearJuego(this.juego));
		this.menues.put(2, new MenuAtaques(this.juego.getMonstruo(1), this.juego.getMonstruo(2)));
		this.menues.put(3, new MenuAtaques(this.juego.getMonstruo(2), this.juego.getMonstruo(1)));
	}

	private IMenu obtenerMenu(int opcion) {
		return this.menues.get(opcion);
	}

	/**
	 * Muestra el menu principal del juego
	 */
	public void mostrarMenu() {

		while (!terminoElJuego()) {

			IMenu menu = null;
			
			while (menu == null) {

				try {

					BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

					// Muestra las opciones del menu
					System.out.println("1- Crear Juego");
					System.out.println("2- Turno Jugador 1");
					System.out.println("3- Turno Jugador 2");

					// Lee la opcion elegida
					int opcion = Integer.parseInt(bufferedReader.readLine());
					menu = this.obtenerMenu(opcion);

					if (menu == null) {
						System.out.println("No es una opcion valida.");
					}

				} catch (IOException ex) {
					System.out.println("Opcion no valida");
				} catch (NumberFormatException ex) {
					System.out.println("Opcion no valida");
				}
			}

			menu.mostrarMenu();
		}

		System.out.println("Fin del juego! Ganador: " + getJugadorGanador());

	}

}
