package menues;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

import interfaces.IMenu;
import juego.Monstruo;
import tiplogias.Tipologia;

public class MenuCrearMonstruos implements IMenu {

	private Monstruo monstruo = null;
	private HashMap<Integer, Tipologia> tipologias;

	public MenuCrearMonstruos(Monstruo monstruo) {
		this.monstruo = monstruo;
		this.tipologias = new HashMap<Integer, Tipologia>();
		llenarTipologias();
	}

	/**
	 * Muestra el menu para crear los monstruos
	 */
	public void mostrarMenu() {
		// Muestra las tipologias y las guarda
		Tipologia tipologia1 = getTipologiaSeleccionada();

		Tipologia tipologia2 = getTipologiaSeleccionada();

		if (tipologia1.equals(tipologia2)) {
			System.out.println("Las tipologias no pueden ser iguales.");
			mostrarMenu();
		} else {
			// Asigna las tipologias
			this.monstruo.setTipologiaPrincipal(tipologia1);
			this.monstruo.setTipologiaSecundaria(tipologia2);
		}
	}

	private Tipologia getTipologiaSeleccionada() {

		Tipologia tipologia = null;

		// Mientras tipologia sea null, va a volver a mostrar las tipologias
		while (tipologia == null) {

			try {
				mostrarTipologias();
				
				// Lee la opcion ingresada y setea la Tipologia en base a la opcion
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
				int opcion = Integer.parseInt(bufferedReader.readLine());

				tipologia = this.obtenerTipologia(opcion);
				
				if (tipologia == null) {
					System.out.println("Opcion no valida.");
				}

			} catch (IOException ex) {
				System.out.println("Opcion no valida.");
			} catch (NumberFormatException ex) {
				System.out.println("La opcion no es un numero.");
			}
		}
		
		return tipologia;
	}

	private void mostrarTipologias() {
		// Muestra el menu con las tipologias disponibles
		System.out.println("Ingrese el numero de la tipologia seleccionada: ");
		System.out.println("1- " + Tipologia.AGUA.toString());
		System.out.println("2- " + Tipologia.AIRE.toString());
		System.out.println("3- " + Tipologia.FUEGO.toString());
		System.out.println("4- " + Tipologia.TIERRA.toString());
	}

	private void llenarTipologias() {
		this.tipologias.put(1, Tipologia.AGUA);
		this.tipologias.put(2, Tipologia.AIRE);
		this.tipologias.put(3, Tipologia.FUEGO);
		this.tipologias.put(4, Tipologia.TIERRA);
	}

	private Tipologia obtenerTipologia(int opcion) {
		return this.tipologias.get(opcion);
	}
}
