package menues;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import interfaces.IMenu;
import juego.Juego;

public class MenuCrearJuego implements IMenu {

	private Juego juego;
	
	public MenuCrearJuego(Juego juego) {
		this.juego = juego;
	}
	
	/**
	 * Muestra el menu para crear el juego
	 */
	public void mostrarMenu() {
		try {
			// Repite 2 veces el pedir los datos del personaje y monstruo
			for (int id = 1; id <= 2; id++) {
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
				
				// Pide los datos del jugador
				System.out.println("Ingrese nombre jugador " + id + ": ");
				String nombre = bufferedReader.readLine();
				
				// Crea el personaje con el nombre ingresado				
				this.juego.getMonstruo(id).setPersonaje(juego.crearPersonaje(nombre));
				
				// Muestra el menu para crear el monstruo
				IMenu menu = new MenuCrearMonstruos(this.juego.getMonstruo(id));
				menu.mostrarMenu();
			}
		} catch (IOException ex) {
			System.out.println("Opcion no valida.");
			mostrarMenu();
		}
	}
	
}
