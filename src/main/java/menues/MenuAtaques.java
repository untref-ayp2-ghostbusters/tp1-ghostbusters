package menues;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import interfaces.IMenu;
import juego.Monstruo;
import tiplogias.Tipologia;

public class MenuAtaques implements IMenu {
	private final String MENSAJE_DANIO_RECIBIDO = "El monstruo enemigo recibio %.2f puntos de da�o.";
	private final String MENSAJE_VIDA_ENEMIGO = "El monstruo enemigo posee %.2f puntos de vida";
	// Personajes como atributos
	private Monstruo monstruoAtacante;
	private Monstruo monstruoEnemigo;

	// Constructor del Menu para Ataques
	public MenuAtaques(Monstruo monstruoAtacante, Monstruo monstruoEnemigo) {
		this.monstruoAtacante = monstruoAtacante;
		this.monstruoEnemigo = monstruoEnemigo;
	}

	/**
	 * Muestra el menu de ataques del juego.
	 */
	public void mostrarMenu() {
		try {
			// Muestra las opciones del menu
			System.out.println("1- Ataque simple tipo: " + monstruoAtacante.getTipologiaPrincipal());
			System.out.println("2- Ataque especial tipo: " + monstruoAtacante.getTipologiaPrincipal());
			System.out.println("3- Ataque simple tipo: " + monstruoAtacante.getTipologiaSecundaria());
			System.out.println("4- Ataque especial tipo: " + monstruoAtacante.getTipologiaSecundaria());
			// Lee la opcion elegida
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
			String opcion = bufferedReader.readLine();

			realizarAtaque(opcion);

		} catch (IOException ex) {
			System.out.println("Opcion no valida");
			mostrarMenu();
		}
	}

	private void ataqueEspecial(Tipologia tipologia) {
		System.out.println(String.format(MENSAJE_DANIO_RECIBIDO,
				monstruoAtacante.ataque(tipologia, monstruoEnemigo)));
		System.out.println(String.format(MENSAJE_VIDA_ENEMIGO, monstruoEnemigo.getPuntosDeVida()));
	}

	private void ataqueSimple() {
		System.out.println(String.format(MENSAJE_DANIO_RECIBIDO, monstruoAtacante.ataque(monstruoEnemigo)));
		System.out.println(String.format(MENSAJE_VIDA_ENEMIGO, monstruoEnemigo.getPuntosDeVida()));
	}

	private void realizarAtaque(String opcion) {

		// Elijo el ataque que voy a realizar
		switch (Integer.parseInt(opcion)) {
		case 1:
		case 3:
			ataqueSimple();
			break;
		case 2:
			ataqueEspecial(monstruoAtacante.getTipologiaPrincipal());
			break;
		case 4:
			ataqueEspecial(monstruoAtacante.getTipologiaSecundaria());
			break;
		default:
			System.out.println("No es una opcion valida.");
			mostrarMenu();
			break;
		}
	}
}
