package juego;

public class Personaje {

	private String nombre = null;

	/**
	 * Crea un personaje con un nombre.
	 * @param String
	 *            nombre
	 */
	public Personaje(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return String nombre
	 */
	public String getNombreDePersonaje() {
		return this.nombre;
	}
}
