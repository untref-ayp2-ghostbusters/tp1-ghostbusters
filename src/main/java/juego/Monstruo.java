package juego;

import tiplogias.Tipologia;

public class Monstruo {

	private static final double ATAQUE_ESPECIAL = 15;
	private static final double ATAQUE_SIMPLE = 10;
	private static final double PORCENTAJE_DANIO_ATAQUE_ESPECIAL = 0.2;
	private int contadorAtaquesEspeciales = 0;
	private double puntosDeVida = 100;
	private Tipologia tipologiaPrincipal;
	private Tipologia tipologiaSecundaria;
	private Personaje personaje;

	// Constructor del monstruo
	public Monstruo() {
	}

	/**
	 * Realiza un ataque simple.
	 * 
	 * @param nombrePersonaje
	 *            nombre del Personaje
	 * @return ATAQUE_SIMPLE
	 */
	public double ataque(Monstruo monstruoEnemigo) {
		monstruoEnemigo.puntosDeVida -= ATAQUE_SIMPLE;

		if (monstruoEnemigo.puntosDeVida < 0) {
			monstruoEnemigo.puntosDeVida = 0;
		}

		return ATAQUE_SIMPLE;
	}

	/**
	 * Realiza un ataque especial.
	 * 
	 * @param tipologiaAtacante
	 *            tipologia del monstruo atacante
	 * @param monstruoEnemigo
	 *            monstruo enemigo
	 * @return valorAtaque cantidad de da�o realizado con el ataque especial.
	 */
	public double ataque(Tipologia tipologiaAtacante, Monstruo monstruoEnemigo) {

		double valorAtaque = ATAQUE_ESPECIAL;

		if (contadorAtaquesEspeciales < 4) {
			if (soyFuerteEnAtaque(tipologiaAtacante, monstruoEnemigo)) {
				valorAtaque += ATAQUE_ESPECIAL * PORCENTAJE_DANIO_ATAQUE_ESPECIAL;
			}
			if (enemigoEsFuerteEnDefensa(tipologiaAtacante, monstruoEnemigo)) {
				valorAtaque -= (ATAQUE_ESPECIAL * PORCENTAJE_DANIO_ATAQUE_ESPECIAL);
			}
			monstruoEnemigo.herir(valorAtaque);
			contadorAtaquesEspeciales++;
		} else {
			valorAtaque = 0;
			System.out.println("Super� el limite de 4 ataques especiales.");
		}
		return valorAtaque;
	}
	
	/** 
	 * Quita vida al monstruo con el valor del ataque que le hayan hecho. Si su vida queda en negativa
	 * se reduce automaticamente a 0.
	 * @param danioRealizado
	 */
	protected void herir(double danioRealizado) {
		this.puntosDeVida -= danioRealizado;
		
		if (this.puntosDeVida < 0) {
			this.puntosDeVida = 0;
		}
	}

	/**
	 * Realiza un ataque especial.
	 * 
	 * @return puntosDeVida puntos de vida restante del monstruo enemigo.
	 */
	public double getPuntosDeVida() {
		return puntosDeVida;
	}

	/**
	 * Devuelve la tipologia del monstruo.
	 * 
	 * @param id
	 *            el indice para saber si es la tipologia 1 o 2
	 * @return devuelve la tipologia correspondiente
	 */
	public Tipologia getTipologiaPrincipal() {
		return tipologiaPrincipal;
	}

	public Tipologia getTipologiaSecundaria() {
		return tipologiaSecundaria;
	}

	/**
	 * Devuelve el personaje due�o de este monstruo.
	 * 
	 * @return
	 */
	public Personaje getPersonaje() {
		return personaje;
	}

	/**
	 * Setea el personaje due�o de este monstruo.
	 * 
	 * @param personaje
	 */
	public void setPersonaje(Personaje personaje) {
		this.personaje = personaje;
	}

	/**
	 * Setea la tipologia principal del monstruo
	 * @param tipologia
	 */
	public void setTipologiaPrincipal(Tipologia tipologia) {
		this.tipologiaPrincipal = tipologia;
	}
	
	/**
	 * Setea la tipologia secundaria del monstruo
	 * @param tipologia
	 */
	public void setTipologiaSecundaria(Tipologia tipologia) {
		this.tipologiaSecundaria = tipologia;
	}

	// Devuelve si el monstruo atacante es fuerte en ataque contra las tipologias
	// del monstruo enemigo.
	private boolean soyFuerteEnAtaque(Tipologia tipologiaAtacante, Monstruo monstruoEnemigo) {

		return monstruoEnemigo.getTipologiaPrincipal() == tipologiaAtacante.getFuerteEnAtaque()
				|| monstruoEnemigo.getTipologiaSecundaria() == tipologiaAtacante.getFuerteEnAtaque();
	}

	// Devuelve si el monstruo enemigo es fuerte en defensa contra las tipologias
	// del monstruo atacante.
	private boolean enemigoEsFuerteEnDefensa(Tipologia tipologiaAtacante, Monstruo monstruoEnemigo) {

		return monstruoEnemigo.getTipologiaPrincipal() == tipologiaAtacante.getFuerteEnDefensa()
				|| monstruoEnemigo.getTipologiaSecundaria() == tipologiaAtacante.getFuerteEnDefensa();
	}

}