package juego;

import menues.MenuInicial;

public class Juego {

	public static void main(String[] args){
		Juego juego = new Juego();
		juego.iniciarJuego();
	}
	
	//Personajes como atributos
	private Monstruo monstruoDos; 
	private Monstruo monstruoUno;
	
	private MenuInicial menuInicial;

	public Juego() {
		this.monstruoUno = new Monstruo();
		this.monstruoDos = new Monstruo();
		this.menuInicial = new MenuInicial(this);
	}
	
	/**
	 * Devuelve un personaje con el nombre parametrizado
	 * @param nombrePersonaje nombre del Personaje
	 * @return Nuevo Personaje
	 */
	public Personaje crearPersonaje(String nombrePersonaje) {
		return new Personaje(nombrePersonaje);
	}

	/**
	 * Devuelve el personaje segun el id paramtetrizado (1 o 2) Sino, devuelve null
	 * @param int id
	 * @return Personaje
	 */
	public Monstruo getMonstruo(int id) {

		Monstruo monstruo = null;

		if (id == 1) {
			monstruo = monstruoUno;
		} else if (id == 2) {
			monstruo = monstruoDos;
		}

		return monstruo;
	}

	/**
	 * Define el personaje 1 o 2 segun su ID
	 * @param personaje
	 * @param id
	 */
	public void setMonstruo(Monstruo monstruo, int id) {

		if (id == 1) {
			this.monstruoUno = monstruo;
		} else if (id == 2) {
			this.monstruoDos = monstruo;
		}
	}
	
	/**
	 * Inicia el juego
	 */
	private void iniciarJuego() {
		menuInicial.mostrarMenu();
	}

}
